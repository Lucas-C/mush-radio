#!/usr/bin/env python3

from os.path import dirname

from livereload import Server
from livereload.handlers import LiveReloadHandler
LiveReloadHandler.DEFAULT_RELOAD_TIME = 0  # do not ignore index.html edition by gen_css_bundle and trigger browser reload
                                           # pending https://github.com/lepture/python-livereload/pull/244
from gen_statics_bundles import gen_css_bundle, gen_js_bundle


SCRIPT_DIR = dirname(__file__)

SERVER = Server()
SERVER.watch(SCRIPT_DIR + '/index.html')
SERVER.watch(SCRIPT_DIR + '/assets/css/main.css', gen_css_bundle)
SERVER.watch(SCRIPT_DIR + '/assets/css/noscript.css')
SERVER.watch(SCRIPT_DIR + '/assets/css/particles.css', gen_css_bundle)
SERVER.watch(SCRIPT_DIR + '/assets/js/main.js', gen_js_bundle)
SERVER.watch(SCRIPT_DIR + '/assets/js/util.js', gen_js_bundle)
SERVER.serve(root=SCRIPT_DIR)
