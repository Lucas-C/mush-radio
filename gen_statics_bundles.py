#!/usr/bin/env python3

# USAGE: rm assets/*/bundle* && ./gen_statics_bundles.py && git add assets/*/bundle*

import re
from hashlib import sha1


CSS_SRCS = (
    'assets/css/fontawesome-all.min.css',
    'assets/css/main.css',
    'assets/css/particles.css',
)
JS_SRCS = (
    # Not enabled / does not work for now, cf. usage in https://github.com/Lucas-C/pelican-mg/blob/master/templates/partials/article-short.html#L5
    # 'assets/js/lazysizes-4.0.0-rc3.min.js',
    # 'assets/js/lazysizes-4.0.0-rc3.noscript.min.js',
    'assets/js/jquery.min.js',
    'assets/js/jquery.dropotron.min.js',
    'assets/js/jquery.scrollex.min.js',
    'assets/js/jquery.scrolly.min.js',
    'assets/js/browser.min.js',
    'assets/js/breakpoints.min.js',
    'assets/js/util.js',
    'assets/js/main.js',
)

def gen_css_bundle():
    short_hash = sha1(b''.join(cat(css_file) for css_file in CSS_SRCS)).hexdigest()[:7]
    css_bundle_filepath = 'assets/css/bundle-{}.css'.format(short_hash)
    with open(css_bundle_filepath, 'wb') as bundle:
        for css_file in CSS_SRCS:
            bundle.write(cat(css_file))
    sed('index.html', 'assets/css/bundle-[a-z0-9]+.css', 'assets/css/bundle-{}.css'.format(short_hash))

def gen_js_bundle():
    short_hash = sha1(b''.join(cat(js_file) for js_file in JS_SRCS)).hexdigest()[:7]
    js_bundle_filepath = 'assets/js/bundle-{}.js'.format(short_hash)
    with open(js_bundle_filepath, 'wb') as bundle:
        for js_file in JS_SRCS:
            bundle.write(cat(js_file))
    sed('index.html', 'assets/js/bundle-[a-z0-9]+.js', 'assets/js/bundle-{}.js'.format(short_hash))

def cat(filepath):
    with open(filepath, 'rb') as f:
        return f.read()

def sed(filepath, pattern, value):
    with open(filepath, 'r+') as f:
        data = f.read()
        f.seek(0)
        f.write(re.sub(pattern, value, data))
        f.truncate()

if __name__ == '__main__':
    print('Generating CSS bundle')
    gen_css_bundle()
    print('Generating JS bundle')
    gen_js_bundle()
