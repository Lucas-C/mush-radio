#!/bin/bash
set -o errexit -o nounset -o pipefail

cd $(dirname "${BASH_SOURCE[0]}")

for jpg in *.jpg; do
    if ! grep -Fq $'\xff\xc2' "$jpg"; then
        jpegoptim --all-progressive -m90 "$jpg"
        chmod 644 "$jpg"
    fi
done

for png in *.png; do
    pngquant --ext .png -f "$png"
    chmod 644 "$png"
done
