# Mush Radio website sources

## Installation

    pip install -r requirements.txt

## Build steps

Insert `width` & `height` attributes on all `<img>` tags
([script source](https://github.com/Lucas-C/dotfiles_and_notes/blob/master/languages/python/auto_set_html_img_width_and_height.py)):

    auto_set_html_img_width_and_height.py index.html

Generate bundles:

    rm assets/*/bundle* && ./gen_statics_bundles.py && git add assets/*/bundle*

This must be done after any change on CSS / JS files, before committing.

This step is automated before every commit using pre-commit hooks:

    pre-commit install

Local development server with auto-reload:

    ./watch_and_serve.py

<!--
# Encore améliorable :
# * réduire le temps de chargement sur mobile (~20s) : https://web.dev/serve-responsive-images/
# * lazy image loading ?
-->